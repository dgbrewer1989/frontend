import { Pipe, PipeTransform } from '@angular/core';
import {
  DAY_FULL_SINGULAR,
  DAY_SHORT, DAYS_PER_MONTH,
  HOUR_FULL_SINGULAR,
  HOUR_SHORT,
  HOURS_PER_DAY,
  MINUTE_FULL_SINGULAR,
  MINUTE_SHORT,
  MINUTES_PER_HOUR, MONTH_FULL_SINGULAR, MONTH_SHORT, MONTHS_PER_YEAR,
  SECOND_FULL_SINGULAR,
  SECOND_SHORT,
  SECONDS_PER_MINUTE, YEAR_FULL_SINGULAR, YEAR_SHORT,
} from '../_constants/time.constants';

@Pipe({
  name: 'timeSpent',
})
// TODO: Clean this pipe, add options to select format
export class TimeSpentPipe implements PipeTransform {

  /**
   * Converts a number to a [X month(s) Y day(s) Z hour(s) ...] format
   * @param timeAmount The time amount to convert, default is MS
   * @param multiplierToSeconds The multiplier required to transform the given unit to seconds
   * @returns The elapsed time in a human readable format
   */
  public transform(timeAmount: number, multiplierToSeconds: number = 0.001, useFullUnit: boolean = true): string {
    let timeDiff: number = timeAmount * multiplierToSeconds;

    // Getting a time unit, then stripping it from the remaining time diff, for each desired unit
    const seconds: number = Math.round(timeDiff % SECONDS_PER_MINUTE);
    timeDiff = Math.floor(timeDiff / SECONDS_PER_MINUTE);
    const minutes: number = Math.round(timeDiff % MINUTES_PER_HOUR);
    timeDiff = Math.floor(timeDiff / MINUTES_PER_HOUR);
    const hours: number = Math.round(timeDiff % HOURS_PER_DAY);
    timeDiff = Math.floor(timeDiff / HOURS_PER_DAY);

    const days: number = Math.round(timeDiff % DAYS_PER_MONTH);
    timeDiff = Math.floor(timeDiff / DAYS_PER_MONTH);

    const months: number = Math.round(timeDiff % MONTHS_PER_YEAR);
    timeDiff = Math.floor(timeDiff / MONTHS_PER_YEAR);

    // the remainder is the number of years
    const years: number = timeDiff;

    const timeUnits: string[] = [];
    if (useFullUnit) {
      timeUnits.push(this.getWordOfUnit(years, YEAR_FULL_SINGULAR));
      timeUnits.push(this.getWordOfUnit(months, MONTH_FULL_SINGULAR));
      timeUnits.push(this.getWordOfUnit(days, DAY_FULL_SINGULAR));
      timeUnits.push(this.getWordOfUnit(hours, HOUR_FULL_SINGULAR));
      timeUnits.push(this.getWordOfUnit(minutes, MINUTE_FULL_SINGULAR));
      timeUnits.push(this.getWordOfUnit(seconds, SECOND_FULL_SINGULAR));
    } else {
      if (years > 0) {
        timeUnits.push(years + YEAR_SHORT);
      }
      if (months > 0) {
        timeUnits.push(months + MONTH_SHORT);
      }
      if (days > 0) {
        timeUnits.push(days + DAY_SHORT);
      }
      if (hours > 0) {
        timeUnits.push(hours + HOUR_SHORT);
      }
      if (minutes > 0) {
        timeUnits.push(minutes + MINUTE_SHORT);
      }
      if (seconds > 0) {
        timeUnits.push(seconds + SECOND_SHORT);
      }
    }

    return timeUnits.join(' ');
  }

  /**
   * Converts a unit and its singular word to a readable format
   * If the unit value is 0, returns an empty string
   * @param unitValue The amount of the given unit
   * @param singularWord The singular of the unit
   * @returns The formatted string
   */
  private getWordOfUnit(unitValue: number, singularWord: string): string {
    if (unitValue > 0) {
      if (unitValue === 1) {
        return `${unitValue} ${singularWord}`;
      } else {
        return `${unitValue} ${singularWord}s`;
      }
    }

    return '';
  }

}
