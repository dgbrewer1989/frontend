import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'elapsed',
})
export class ElapsedPipe implements PipeTransform {

  /**
   * Converts a timestamp to the number of MS since this event
   * @param timeAmount The time amount to convert, default is MS
   * @param msMultiplier The multiplier required to convert the time to MS
   */
  public transform(timeAmount: number, msMultiplier: number = 1): number {
    const currentTime: number = Date.now();

    return currentTime - (timeAmount * msMultiplier);
  }

}
