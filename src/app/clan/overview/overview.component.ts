import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ActivatedRoute, Params} from '@angular/router';
import {TdDialogService} from '@covalent/core';
import {IClan, IClanMember, TClanRank} from 'brawlhalla-api-ts-interfaces';
import {ClanService} from '../../_services/clan.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  providers: [ClanService],
})
export class OverviewComponent implements OnInit {

  public clanId: number;
  public clan?: IClan;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialogService: TdDialogService,
    private readonly clanService: ClanService,
    private readonly meta: Meta,
    private readonly title: Title,
  ) {
    this.updateSEO();
  }

  public ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const clanId: number = parseInt(params['id'], 10);
      if (isNaN(clanId)) {
        this.dialogService.openAlert({
          title: 'An error has occurred',
          message: 'The selected clan is invalid!',
        });
      } else {
        this.clanId = clanId;
        this.selectClan(clanId)
          .then(() => {
            this.updateSEO();
          });
      }
    });
  }

  public async selectClan(id: number): Promise<void> {
    this.clan = undefined;
    this.clan = await this.clanService.getById(id).toPromise();
  }

  public get leader(): IClanMember {
    return this.getMembersOfRank('Leader')[0];
  }

  public get officers(): IClanMember[] {
    return this.getMembersOfRank('Officer');
  }

  public get members(): IClanMember[] {
    return this.getMembersOfRank('Member');
  }

  public get recruits(): IClanMember[] {
    return this.getMembersOfRank('Recruit');
  }

  private getMembersOfRank(rank: TClanRank): IClanMember[] {
    return this.clan.members.filter((member: IClanMember) => member.rank === rank);
  }

  private updateSEO(): void {
    if (!this.clan) {
      this.title.setTitle('Clan information - Loading - BrawlDB');
      this.meta.updateTag({
        name: 'description',
        content: 'Information about a brawlhalla clan',
      });
    } else {
      this.title.setTitle(`${this.clan.name} - Clan information - BrawlDB`);
      this.meta.updateTag({
        name: 'description',
        content: `Information about ${this.clan.name}, a brawlhalla clan`,
      });
    }
  }
}
