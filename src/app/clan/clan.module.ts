import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule, MatDividerModule, MatGridListModule, MatProgressSpinnerModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { CovalentDialogsModule } from '@covalent/core';
import { PipesModule } from '../_pipes/pipes.module';
import { MemberComponent } from './member/member.component';
import { OverviewComponent } from './overview/overview.component';

const clanRoutes: Routes = [
  { path: 'clan/info/:id', component: OverviewComponent },
];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(clanRoutes),
    /** Material Modules */
    MatCardModule,
    MatDividerModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    /** Covalent Modules */
    CovalentDialogsModule,
  ],
  exports: [
    RouterModule,
  ],
  declarations: [
    OverviewComponent,
    MemberComponent,
  ],
})
export class ClanModule {
}
