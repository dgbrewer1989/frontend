import { Injectable } from '@angular/core';
import {IPlayerRankedStats, IPlayerRanking, IPlayerStats, TValidRegions} from 'brawlhalla-api-ts-interfaces';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CachedHttpClient } from './cached-http-client.service';

/**
 * Performs various requests to access Brawlhalla player data
 */
@Injectable()
export class PlayerService {

  /**
   * The endpoint for the player api
   */
  private static PLAYER_SERVICE_URL: string = `${environment.backend_url}/player`;

  constructor(private readonly http: CachedHttpClient) {
  }

  /**
   * Gets the player rankings based on a username
   * @param  username The username to search
   * @returns The resulting rankings in an observable
   */
  public getPlayersByName(username: string): Observable<IPlayerRanking[]> {
    return this.http.get<IPlayerRanking[]>(`${PlayerService.PLAYER_SERVICE_URL}/ranking/${username}`);
  }

  /**
   * Gets the player stats based on his BHID
   * @param bhid The player's brawlhalla identifier
   * @returns The player stats in an observable
   */
  public getPlayerStatsById(bhid: number): Observable<IPlayerStats> {
    return this.http.get<IPlayerStats>(`${PlayerService.PLAYER_SERVICE_URL}/id/${bhid}`);
  }

  /**
   * Gets the player ranked stats based on his BHID
   * @param bhid The player's brawlhalla identifier
   * @returns The player ranked stats in an observable
   */
  public getPlayerRankedStatsById(bhid: number): Observable<IPlayerRankedStats> {
    return this.http.get<IPlayerRankedStats>(`${PlayerService.PLAYER_SERVICE_URL}/ranked/id/${bhid}`);
  }

  public getPlayerRankings(
    region: TValidRegions,
    bracket: string,
    page: number,
    name: string,
  ): Observable<IPlayerRanking[]> {
    return this.http.get<IPlayerRanking[]>(
      `${PlayerService.PLAYER_SERVICE_URL}/ranking/${bracket}/${region}/${page}/${name}`,
    );
  }
}
