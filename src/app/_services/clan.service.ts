import { Injectable } from '@angular/core';
import { IClan } from 'brawlhalla-api-ts-interfaces';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { CachedHttpClient } from './cached-http-client.service';

/**
 * Performs various requests to access Brawlhalla clan data
 */
@Injectable()
export class ClanService {

  /**
   * The endpoint for the clan api
   */
  private static CLAN_SERVICE_URL: string = `${environment.backend_url}/clan`;

  constructor(private readonly http: CachedHttpClient) {}

  /**
   * Gets clan information based on its id
   * @param  id The id to search
   * @returns The resulting clan information
   */
  public getById(id: number): Observable<IClan> {
    return this.http.get<IClan>(`${ClanService.CLAN_SERVICE_URL}/id/${id}`);
  }
}
