export const RED: string = '#ff111d';
export const GREEN: string = '#00db01';
export const DARK_RED: string = '#b70007';
export const DARK_GREEN: string = '#399136';
