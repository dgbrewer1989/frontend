import {TValidRegions} from 'brawlhalla-api-ts-interfaces';

export const VALID_REGIONS: TValidRegions[] = ['all', 'us-e', 'us-w', 'eu', 'sea', 'brz', 'aus'];

