import {Component, Input} from '@angular/core';
import {IPlayerRanking} from 'brawlhalla-api-ts-interfaces';
import {DARK_GREEN, DARK_RED, GREEN, RED} from '../../_constants/chart.constants';
import {IChartColor, IChartData} from '../../_interfaces/chart.interface';

@Component({
  selector: 'app-player-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css'],
})
export class PreviewComponent {
  @Input()
  public player: IPlayerRanking;

  public get colorScheme(): IChartColor {
    return {
      domain: !this.isBestEloLegend ? [GREEN, RED] : [GREEN, DARK_GREEN, RED, DARK_RED],
    };
  }

  public get isBestEloLegend(): boolean {
    return !(this.player.bestLegendGames === this.player.games && this.player.bestLegendWins === this.player.wins);
  }

  public get chartData(): IChartData[] {
    const bestLegendWins: number = this.player.bestLegendWins;
    const bestLegendDefeats: number = this.player.bestLegendGames - bestLegendWins;
    const otherLegendWins: number = this.player.wins - bestLegendWins;
    const otherLegendDefeats: number = this.player.games - this.player.wins - bestLegendDefeats;
    let result: IChartData[];
    if (!this.isBestEloLegend) {
      result = [
        {
          name: 'Victories',
          value: bestLegendWins,
        },
        {
          name: 'Defeats',
          value: bestLegendDefeats,
        },
      ];
    } else {
      result = [
        {
          name: `${this.player.bestLegend.name} victories`,
          value: bestLegendWins,
        },
        {
          name: 'Other victories',
          value: otherLegendWins,
        },
        {
          name: `${this.player.bestLegend.name} defeats`,
          value: bestLegendDefeats,
        },
        {
          name: 'Other defeats',
          value: otherLegendDefeats,
        },
      ];
    }
    return result;
  }
}
