import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {IPlayerRanking, TValidRegions} from 'brawlhalla-api-ts-interfaces';
import {VALID_REGIONS} from '../../_constants/region.constants';
import {PlayerService} from '../../_services/player.service';
import {ITdDataTableColumn} from '@covalent/core';

@Component({
  selector: 'app-player-rankings',
  templateUrl: './player-rankings.component.html',
  styleUrls: ['./player-rankings.component.css'],
  providers: [ PlayerService ],
})
export class PlayerRankingsComponent {
  public readonly regions: TValidRegions[] = VALID_REGIONS;

  public selectedPlayer: IPlayerRanking;
  public players: IPlayerRanking[];
  public region: TValidRegions;

  public page: number;

  public columns: ITdDataTableColumn[] = [
    { name: 'rank',  label: 'Rank', numeric: true },
    { name: 'name',  label: 'Rank' },
    { name: 'tier',  label: 'Tier' },
    { name: 'games',  label: 'Games' },
    { name: 'wins',  label: 'Wins' },
    { name: 'bestLegend.name',  label: 'Main' },
    { name: 'rating',  label: 'Rating' },
    { name: 'peakRating', label: 'Peak' },
    { name: 'region', label: 'Region' },
  ];

  private bracket: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly title: Title,
    private readonly meta: Meta,
    private readonly playerService: PlayerService,
  ) {
    this.updatePageData(this.route.snapshot.paramMap);
    this.route.paramMap.subscribe((routeData: ParamMap) => {
      this.updatePageData(routeData);
    });
  }

  public updateRoute(): void {
    this.router.navigate(['player', 'rankings', this.bracket, this.region, this.page]);
  }

  public selectPlayer(player: IPlayerRanking): void {
    this.selectedPlayer = player;
  }

  public log(thing: any): void {
    console.log(thing);
  }

  private updatePageData(routeData: ParamMap): void {
    this.players = undefined;
    this.selectedPlayer = undefined;
    this.region = this.getDataOrFallback(
      routeData,
      'region',
      'all',
      (desiredRegion: string) => VALID_REGIONS.includes(desiredRegion as TValidRegions),
    ) as TValidRegions;

    if (this.region == "all") {
      this.columns.find(region => region.name == 'region').hidden = false;
    }
    else {
      this.columns.find(region => region.name == 'region').hidden = true;
    }

    this.bracket = this.getDataOrFallback(
      routeData,
      'bracket',
      '1v1',
      (desiredBracket: string) => desiredBracket === '1v1',
    );

    this.page = +this.getDataOrFallback(
      routeData,
      'page',
      '1',
      (desiredPage: string) => parseInt(desiredPage, 10) > 0,
    );
    this.updateSEO();
    this.updatePlayers();
  }

  private async updatePlayers(): Promise<void> {
    this.players = await this.playerService.getPlayerRankings(
      this.region,
      this.bracket,
      this.page,
      '',
    ).toPromise();
  }

  private getDataOrFallback(params: ParamMap,
                            key: string,
                            fallback: string,
                            validator: (key: string) => boolean): string {
    let result: string = fallback;
    if (params.has(key)) {
      const desiredValue: string = params.get(key);
      if (validator(desiredValue)) {
        result = desiredValue;
      }
    }

    return result;
  }

  private updateSEO(): void {
    this.title.setTitle(`Brawhalla rankings: ${this.bracket} ${this.region} page ${this.page}`);
  }

}
