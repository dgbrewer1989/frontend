import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRankedHeaderComponent } from './player-ranked-header.component';

describe('PlayerRankedHeaderComponent', () => {
  let component: PlayerRankedHeaderComponent;
  let fixture: ComponentFixture<PlayerRankedHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerRankedHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRankedHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
