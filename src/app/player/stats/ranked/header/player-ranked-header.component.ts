import {Component, Input} from '@angular/core';
import {ILegendRankedStats, IPlayerRankedStats} from 'brawlhalla-api-ts-interfaces';
import {DARK_GREEN, DARK_RED} from '../../../../_constants/chart.constants';
import { IChartMultipleData } from '../../../../_interfaces/chart.interface';

@Component({
  selector: 'app-player-ranked-header',
  templateUrl: './player-ranked-header.component.html',
  styleUrls: ['./player-ranked-header.component.css'],
})
export class PlayerRankedHeaderComponent {
  @Input() playerRankedStats: IPlayerRankedStats;

  public get chartColors(): { domain: string[] } {
    return {
      domain: [
        DARK_GREEN, DARK_RED,
      ],
    };
  }

  public get games(): number {
    return this.playerRankedStats.legends.reduce(
      (acc: number, legend: ILegendRankedStats) => {
        return acc + legend.games;
      },
      0);
  }

  public get victories(): number {
    return this.playerRankedStats.legends.reduce(
      (acc: number, legend: ILegendRankedStats) => {
        return acc + legend.wins;
      },
      0);
  }

  public get defeats(): number {
    return this.playerRankedStats.legends.reduce(
      (acc: number, legend: ILegendRankedStats) => {
        return acc + (legend.games - legend.wins);
      },
      0);
  }

  public get rankedWinChartData(): IChartMultipleData[] {
    return [
      {
        name: 'Games',
        series: [
          {
            name: 'Victories',
            value: this.victories,
          },
          {
            name: 'Defeats',
            value: this.defeats,
          },
        ],
      },
    ];
  }
}
