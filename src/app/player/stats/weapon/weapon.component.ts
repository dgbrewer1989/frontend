import { Component, Input } from '@angular/core';
import { IWeaponStats } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-weapon',
  templateUrl: './weapon.component.html',
  styleUrls: ['./weapon.component.css'],
})
export class WeaponComponent {

  @Input() public weaponStats: IWeaponStats[];

  private get orderedWeaponStats(): IWeaponStats[] {
    return this.weaponStats.sort((a: IWeaponStats, b: IWeaponStats) => {
      return b.games - a.games;
    });
  }

  public getBestNthWeapon(index: number): IWeaponStats {
    return this.orderedWeaponStats[ index ];
  }
}
