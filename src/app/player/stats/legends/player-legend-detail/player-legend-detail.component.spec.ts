import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerLegendDetailComponent } from './player-legend-detail.component';

describe('PlayerLegendDetailComponent', () => {
  let component: PlayerLegendDetailComponent;
  let fixture: ComponentFixture<PlayerLegendDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerLegendDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerLegendDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
