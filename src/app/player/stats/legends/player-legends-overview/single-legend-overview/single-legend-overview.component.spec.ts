import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleLegendOverviewComponent } from './single-legend-overview.component';

describe('SingleLegendOverviewComponent', () => {
  let component: SingleLegendOverviewComponent;
  let fixture: ComponentFixture<SingleLegendOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleLegendOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleLegendOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
