import { Component, Input, OnInit } from '@angular/core';
import { ILegendStats } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-single-legend-overview',
  templateUrl: './single-legend-overview.component.html',
  styleUrls: ['./single-legend-overview.component.css']
})
export class SingleLegendOverviewComponent {
  @Input() legend: ILegendStats;
}
