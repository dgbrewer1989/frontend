import { Component, Input } from '@angular/core';
import { ILegendRankedStats, ILegendStats } from 'brawlhalla-api-ts-interfaces';

@Component({
  selector: 'app-player-legends-overview',
  templateUrl: './player-legends-overview.component.html',
  styleUrls: [ './player-legends-overview.component.css' ],
})
export class PlayerLegendsOverviewComponent {
  @Input() legendStats: ILegendStats[];
  @Input() legendRankedStats?: ILegendRankedStats[];

  private get orderedLegendStats(): ILegendStats[] {
    return this.legendStats.sort((a: ILegendStats, b: ILegendStats) => {
      return b.matchTime - a.matchTime;
    });
  }

  public getBestNthLegend(index: number): ILegendStats {
    return this.orderedLegendStats[ index ];
  }
}
