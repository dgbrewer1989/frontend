import { Component, Input } from '@angular/core';
import { IPlayerStats } from 'brawlhalla-api-ts-interfaces';
import { DARK_GREEN, DARK_RED } from '../../../_constants/chart.constants';
import { IChartColor, IChartData } from '../../../_interfaces/chart.interface';
import { getDamageDealt, getDamageTaken, getDefeats, getFalls, getKos, getVictories } from '../../../_utils/player-stats.util';

@Component({
  selector: 'app-player-stats-overview',
  templateUrl: './player-stats-overview.component.html',
  styleUrls: [ './player-stats-overview.component.css' ],
})
export class PlayerStatsOverviewComponent {
  @Input() stats: IPlayerStats;

  public get chartColors(): IChartColor {
    return {
      domain: [ DARK_GREEN, DARK_RED ],
    };
  }

  public get winData(): IChartData[] {
    return [
      {
        name: 'Victories',
        value: getVictories(this.stats),
      },
      {
        name: 'Defeats',
        value: getDefeats(this.stats),
      },
    ];
  }

  public get damageData(): IChartData[] {
    return [
      {
        name: 'Damage Dealt',
        value: getDamageDealt(this.stats),
      },
      {
        name: 'Damage Taken',
        value: getDamageTaken(this.stats),
      },
    ];
  }

  public get koData(): IChartData[] {
    return [
      {
        name: 'Kos',
        value: getKos(this.stats),
      },
      {
        name: 'Falls',
        value: getFalls(this.stats),
      },
    ];
  }
}
