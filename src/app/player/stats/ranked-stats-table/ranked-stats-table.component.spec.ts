import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankedStatsTableComponent } from './ranked-stats-table.component';

describe('RankedStatsTableComponent', () => {
  let component: RankedStatsTableComponent;
  let fixture: ComponentFixture<RankedStatsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankedStatsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankedStatsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
