import { Component, Input } from '@angular/core';
import { IPlayerTeamStats } from 'brawlhalla-api-ts-interfaces';
import { IChartMultipleData } from '../../../../_interfaces/chart.interface';
import { DARK_GREEN, DARK_RED } from '../../../../_constants/chart.constants';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: [ './team.component.css' ],
})
export class TeamComponent {

  @Input() public team: IPlayerTeamStats;
  @Input() public currentPlayerId: number;

  public get otherPlayerName(): string {
    if (this.currentPlayerId === this.team.brawlhallaIdOne) {
      return this.team.brawlhallaNameTwo;
    } else {
      return this.team.brawlhallaNameOne;
    }
  }

  public get otherPlayerId(): number {
    if (this.currentPlayerId === this.team.brawlhallaIdOne) {
      return this.team.brawlhallaIdTwo;
    } else {
      return this.team.brawlhallaIdOne;
    }
  }

  public get teamGamesChartData(): IChartMultipleData[] {
    return [
      {
        name: 'Games',
        series: [
          {
            name: 'Victories',
            value: this.team.wins,
          },
          {
            name: 'Defeats',
            value: this.team.games - this.team.wins,
          },
        ],
      },
    ];
  }

  public get chartColors(): { domain: string[] } {
    return {
      domain: [
        DARK_GREEN, DARK_RED,
      ],
    };
  }

}
