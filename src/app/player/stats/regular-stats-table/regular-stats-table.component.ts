import {Component, Input} from '@angular/core';
import {ILegendStats, IPlayerStats} from 'brawlhalla-api-ts-interfaces';
import {ITdDataTableColumn} from '@covalent/core';

@Component({
  selector: 'app-player-regular-stats-table',
  templateUrl: './regular-stats-table.component.html',
  styleUrls: ['./regular-stats-table.component.css'],
})
export class RegularStatsTableComponent {
  @Input() stats: IPlayerStats;

  public get columns(): ITdDataTableColumn[] {
    return [
      {name: 'legend.name', label: 'Legend Name'},
      {name: 'level', label: 'Level'},
      {name: 'games', label: 'Games'},
      {name: 'wins', label: 'Wins'},
      {name: 'damageDealt', label: 'Damage dealt'},
      {name: 'damageTaken', label: 'Damage taken'},
      {name: 'kos', label: 'KOs'},
      {name: 'falls', label: 'Falls'},
      {name: 'suicides', label: 'Suicides'},
      {name: 'teamKos', label: 'Team KO\'s'},
    ];
  }

  /**
   xp: number;
   xpPercentage: number;
   matchTime: number;
   games: number;
   wins: number;
   defeats: number;
   damageDealtByUnarmed: number;
   damageDealtByThrownItems: number;
   damageDealtByFirstWeapon: number;
   damageDealtBySecondWeapon: number;
   damageDealtByGadgets: number;
   koByUnarmed: number;
   koByThrownItems: number;
   koByFirstWeapon: number;
   koBySecondWeapon: number;
   koByGadgets: number;
   timeHeldFirstWeapon: number;
   timeHeldSecondWeapon: number;
   */

  public get data(): ILegendStats[] {
    return this.stats.legendStats;
  }
}
