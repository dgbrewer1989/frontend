import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TdDialogService} from '@covalent/core';
import {IPlayerRankedStats, IPlayerStats} from 'brawlhalla-api-ts-interfaces';
import {PlayerService} from '../../_services/player.service';

@Component({
  selector: 'app-player-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [PlayerService],
})
export class PlayerStatsComponent implements OnInit {

  public playerId: number;
  public playerStats: IPlayerStats;
  public playerRankedStats: IPlayerRankedStats;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialogService: TdDialogService,
    private readonly playerService: PlayerService,
    private readonly title: Title,
    private readonly meta: Meta,
  ) {
    this.updateSEO();
  }

  public ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const playerId: number = parseInt(params['id'], 10);
      // Temporary solution to support the previous version of the routes
      const oldPlayerId: number = parseInt(this.route.snapshot.queryParams['bhId'], 10);
      if (!isNaN(oldPlayerId)) {
        this.router.navigate(['player', 'stats', oldPlayerId]);
      } else if (isNaN(playerId)) {
        this.showErrorMessage('The selected player is invalid!');
      } else {
        this.playerId = playerId;
        this.selectPlayer(playerId).then(() => {
          this.updateSEO();
        });
      }
    });
  }

  public async selectPlayer(id: number): Promise<void> {
    // Resetting the player, if we're switching between two players
    this.playerStats = undefined;
    this.playerRankedStats = undefined;
    this.playerStats = await this.playerService.getPlayerStatsById(id)
      .toPromise();
    this.playerRankedStats = await this.playerService.getPlayerRankedStatsById(id)
      .toPromise();
  }

  public showErrorMessage(message: string): void {
    this.dialogService.openAlert({
      title: 'An error has occurred',
      message,
    });
  }

  private updateSEO(): void {
    if (!this.playerStats) {
      this.title.setTitle('Player information - Loading - BrawlDB');
      this.meta.updateTag({
        name: 'description',
        content: 'Information about a brawlhalla player',
      });
    } else {
      this.title.setTitle(`${this.playerStats.name} - Player information - BrawlDB`);
      this.meta.updateTag({
        name: 'description',
        content: `Information about ${this.playerStats.name}, a brawlhalla player`,
      });
    }
  }

}
